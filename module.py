import os, shutil, zipfile, distutils.core, json

default_dir = os.path.expanduser("~/Magiskfy")

class Module():
    def __init__(self, module_id, module_name, author, version, version_code, description, replace_dir="", working_dir=default_dir):
        self.id = module_id
        self.name = module_name
        self.author = author
        self.version = version
        self.version_code = version_code
        self.description = description
        self.replace_dir = replace_dir

        self.working_dir = working_dir
        self.module_path = os.path.join(working_dir, os.path.join("Modules", self.name))
        self.module_files = os.path.join(self.module_path, "files")


    def update(self, new_version):
        self.version = new_version
        self.version_code = self.version_code + 1
        output_dir = os.path.join(self.working_dir, "Output")

        self.save_module_info()
        self.apply_config()
        self.makeZip(output_dir)


    def create(self):
        if not os.path.exists(self.module_path):
            os.makedirs(self.module_path)
        else:
            return "Module folder already exists."

        output_dir = os.path.join(self.working_dir, "Output")

        self.apply_input_files()
        self.save_module_info()
        self.apply_config()
        self.makeZip(output_dir)


    def apply_input_files(self):
        temporary_dir = os.path.join(self.working_dir, ".temp")
        template_dir = os.path.join(self.working_dir, "Template")
        input_dir = os.path.join(self.working_dir, "Input")

        os.makedirs(temporary_dir)
        distutils.dir_util.copy_tree(template_dir, temporary_dir)
        distutils.dir_util.copy_tree(input_dir, temporary_dir)
        distutils.dir_util.copy_tree(temporary_dir, self.module_files)

        shutil.rmtree(temporary_dir)
        shutil.rmtree(input_dir)
        os.makedirs(input_dir)


    def apply_config(self):
        with open(os.path.join(self.module_files, "config.sh"), "w", newline="\n") as configFile:
            configFile.write('AUTOMOUNT=true\nPROPFILE=false\nPOSTFSDATA=false\nLATESTARTSERVICE=false\n\nprint_modname() {\n  ui_print " "\n  ui_print " - ' + self.name + ' - "\n  ui_print " "\n}\n\nREPLACE="\n' + self.replace_dir + '\n"\n\nset_permissions() {\nset_perm_recursive  $MODPATH  0  0  0755  0644\n}\n')

        with open(os.path.join(self.module_files, "module.prop"), "w", newline="\n") as moduleprop:
            moduleprop.write("id=" + self.id + "\nname=" + self.name + "\nversion=" + self.version + "\nversionCode=" + str(self.version_code) + "\nauthor=" + self.author + "\ndescription=" + self.description + "\nminMagisk=17000")
        
    
    def makeZip(self, destpath):
        length = len(self.module_files)
        with zipfile.ZipFile(os.path.join(destpath, self.name + " - " + self.version + '.zip'), mode='w') as zFile:
            for root, dirs, files in os.walk(self.module_files):
                folder = root[length:]
                for x in files:
                    zFile.write(os.path.join(root, x), os.path.join(folder, x))

    
    def save_module_info(self):
        module_info = {'config':[{
            'id': self.id,
            'name': self.name,
            'author': self.author,
            'version': self.version,
            'version_code': self.version_code,
            'description': self.description,
            'replace_dir': self.replace_dir
            }]}

        with open(os.path.join(self.module_path, "info.json"), 'w') as infofile:
            json.dump(module_info, infofile, indent=4)
