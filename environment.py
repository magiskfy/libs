import os, shutil, urllib.request, zipfile, distutils.core, json

# defaults
default_dir = os.path.expanduser("~/Magiskfy")
TEMPLATE_URL = "https://github.com/topjohnwu/magisk-module-template/archive/17000.zip"
TEMPLATE_INFOLDER = "magisk-module-template-17000"

class env():
    def __init__(self, author=str(), working_dir=default_dir):
        self.working_dir = working_dir
        self.author = author
        self.input_dir = os.path.join(self.working_dir, "Input")
        self.output_dir = os.path.join(self.working_dir, "Output")
        self.template_dir = os.path.join(self.working_dir, "Template")
        self.modules_dir = os.path.join(self.working_dir, "Modules")
        self.temporary_dir = os.path.join(self.working_dir, ".temp")

    
    def setup(self):
        self.check_folders()
        self.set_settings()
        if not self.check_template():
            self.download_template(TEMPLATE_URL)


    def toggle_custom_dir(self, custom_dir = ""):
        if (custom_dir != ""):
            self.working_dir = custom_dir


    def check_folders(self):
        toCreate = list()
        if not os.path.exists(self.working_dir):
            toCreate.append(self.working_dir)
        if not os.path.exists(self.modules_dir):
            toCreate.append(self.modules_dir)
        if not os.path.exists(self.input_dir):
            toCreate.append(self.input_dir)
        if not os.path.exists(self.output_dir):
            toCreate.append(self.output_dir)
        
        if toCreate:
            self.create_folder(toCreate)

    def create_folder(self, toCreate = list()):
        if isinstance(toCreate, list):
            for x in toCreate:
                if not os.path.exists(x):
                    os.makedirs(x)

        if isinstance(toCreate, str):
            if not os.path.exists(toCreate):
                os.makedirs(toCreate)


    def set_settings(self):
        data = {'settings':[{
            'files': self.working_dir,
            'author': self.author
        }]}
        with open(os.path.join(os.path.expanduser("~"), "magiskfy.json"), 'w') as configfile:
            json.dump(data, configfile, indent=4)   


    def check_template(self):
        template_dir = os.path.join(self.working_dir, "Template")
        if not os.path.exists(template_dir):
            os.makedirs(template_dir)
            return False
        return True


    def download_template(self, url, autoextract=True):
        self.create_folder(self.temporary_dir)
        try:
            with urllib.request.urlopen(url) as response, open(os.path.join(self.temporary_dir, "template.zip"), "wb") as templatefile:
                shutil.copyfileobj(response, templatefile)
            if autoextract == True:
                self.setup_template()
        except urllib.error.URLError:
            return "Connection error"


    def setup_template(self, infolder = TEMPLATE_INFOLDER):
        template_dir = os.path.join(self.working_dir, "Template")
        temporary_dir = os.path.join(self.working_dir, ".temp")
        infolder = os.path.join(temporary_dir, infolder)
        self.create_folder(template_dir)
        with zipfile.ZipFile(os.path.join(temporary_dir, "template.zip"), "r") as templatefile:
            templatefile.extractall(path=temporary_dir)
        distutils.dir_util.copy_tree(infolder, template_dir)
        shutil.rmtree(temporary_dir, True)
        os.remove(os.path.join(template_dir, os.path.join("system", "placeholder")))

    def get_saved_modules(self):
        content = []
        for x in os.listdir(self.modules_dir):
            content.append(x)
        return content
    